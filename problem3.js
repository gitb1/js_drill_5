  // From the given data, filter all the unique langauges and return in an array
function getUniqueLanguages(dataSet) {
    try {
       
        const allLanguages = dataSet.reduce((acc, person) => {
            return acc.concat(person.languages);
        }, []);

        const uniqueLanguages = Array.from(new Set(allLanguages));

        return uniqueLanguages;
    } catch (error) {
        console.error("An error occurred:", error.message);
        return [];
    }
}

module.exports = getUniqueLanguages;
