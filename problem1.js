 // Iterate over each object in the array and in the email breakdown the email and return the output as below:

function getEmailDetails(dataSet) {
    try {
            
       return dataSet.map(person => {
        // Split email address by "@" to get the username and domain
        const [username, domain] = person.email.split("@");
  
        // Split username by "." to get first and last name
        const [firstName, lastName] = username.split(".").map(name => name.charAt(0).toUpperCase() + name.slice(1));
  
        return {
           firstName,
           lastName,
            emailDomain: domain
        };
      });
     }catch(error){
        console.error("An error occurred:", error.message);
     }
   }
  
  module.exports = getEmailDetails;