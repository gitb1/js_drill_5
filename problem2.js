
// Check the each person projects list and group the data based on the project status and return the data as below
  
  function groupProjectsByStatus(dataSet) {
        try {
         
          const groupedProjects = dataSet.reduce((acc, person) => {
            person.projects.forEach(project => {
                const { name, status } = project;
                acc[status] = acc[status] || [];
                acc[status].push(name);
            });
            return acc;
        }, {});
        
          return groupedProjects;
        } catch (error) {
          console.error("An error occurred:", error.message);
          return {};
        }
      }
      
      module.exports = groupProjectsByStatus;
      